<?php
/**
* Setup Timber environment 
*/

function themeprefix_add_to_twig( $twig ) {
  
  $twig->addFilter( new Twig_SimpleFilter( 'merge_recursive', 'themeprefix_array_merge_recursive' ) );
  $twig->addFilter( new Twig_SimpleFilter( 'slugify', 'themeprefix_string_slugify' ) );
  $twig->addFunction( new Timber\Twig_Function('get_background_gradient_color', 'get_background_gradient_color_in_php'));

  return $twig;
}

add_filter( 'timber/twig', 'themeprefix_add_to_twig' );



function themeprefix_array_merge_recursive( $arr1, $arr2 ) {
  if ( $arr1 instanceof Traversable ) {
    $arr1 = iterator_to_array( $arr1 );
  } elseif ( !is_array( $arr1 ) ) {
    throw new Twig_Error_Runtime( sprintf( 'The merge_recursive filter only works with arrays or "Traversable", got "%s" as first argument.', gettype( $arr1 ) ) );
  }
  if ( $arr2 instanceof Traversable ) {
    $arr2 = iterator_to_array( $arr2 );
  } elseif ( !is_array( $arr2 ) ) {
    throw new Twig_Error_Runtime( sprintf( 'The merge_recursive filter only works with arrays or "Traversable", got "%s" as second argument.', gettype( $arr2 ) ) );
  }
  return array_replace_recursive( $arr1, $arr2 );
}



function themeprefix_string_slugify( $str ) {
  if ( gettype( $str ) === 'string' ) {
    $result = strtolower( str_replace( ' ', '-', $str ) );
  } else {
    throw new Twig_Error_Runtime( sprintf( 'The slugify filter only works with strings, got "%s" as first argument.', gettype( $str ) ) );
  }
  return $result;
}

function get_background_gradient_color_in_php($color){
  $bg_arry = [
    'purple' => 'linear-gradient(231.01deg, #E75585 -10.69%, #3D85F5 103.39%);',
    'gold' => 'linear-gradient(45deg, #F0CE2F 0%, #F4A53B 100%, #F4A53B 100%);',
    'green' => 'linear-gradient(236deg, #7ECBB0 6.56%, #F1E735 93.44%);',
    'orange' => 'linear-gradient(236deg, #EB1F4F 6.56%, #FB912F 93.44%);'
  ];

  return $bg_arry[$color];

}