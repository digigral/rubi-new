<?php
/** 
* Registering scripts and styles
*/

function themeprefix_script_enqueuer() {
  $scriptdeps_site = [];

  wp_register_script( 'site', get_template_directory_uri() . '/js/scripts.min.js', array('jquery'), false, true );
  wp_enqueue_script( 'site' );


  wp_register_script( 'ScrollTrigger','https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/ScrollTrigger.min.js',$scriptdeps_site, false, true );
  wp_enqueue_script( 'ScrollTrigger' );

  wp_register_script( 'Draggable','https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/Draggable.min.js',$scriptdeps_site, false, true );
  wp_enqueue_script( 'Draggable' );
  
  wp_register_script( 'gsap','https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/gsap.min.js',$scriptdeps_site, false, true );
  wp_enqueue_script( 'gsap' );

  
  wp_register_script( 'slick','//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',$scriptdeps_site, false, true );
  wp_enqueue_script( 'slick' );

  wp_localize_script( 'site', 'localized_strings', themeprefix_localized_strings() );
  wp_localize_script( 'site', 'script_data', themeprefix_script_data() );

  wp_register_style( 'screen', get_stylesheet_directory_uri() . '/style.css', '', '', 'screen' );
  wp_enqueue_style( 'screen' );

  wp_register_style( 'slick-style', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', '', '', 'screen' );
  wp_enqueue_style( 'slick-style' );
}

add_action( 'wp_enqueue_scripts', 'themeprefix_script_enqueuer', 10 );



function themeprefix_defer_scripts( $tag, $handle, $src ) {

  // The handles of the enqueued scripts we want to defer
  $defer_scripts = [];

  if ( in_array( $handle, $defer_scripts ) ) {
    return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>\n';
  }
  
  return $tag;
}

add_filter( 'script_loader_tag', 'themeprefix_defer_scripts', 10, 3 );
