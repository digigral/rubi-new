'use strict';
import { addBrowserClasses } from './modules/_utils';
import { example_module } from './modules/example-module';
//import { accordion } from './modules/accordion';

document.addEventListener('DOMContentLoaded', function () {

  document.getElementsByClassName('is-page-loading')[0].classList.remove('is-page-loading');
  document.getElementsByClassName('no-js')[0].classList.remove('no-js');

  // Common functions
  addBrowserClasses();

  // Import Javascript modules here:
  example_module();
  //accordion();

  gsap.registerPlugin(ScrollTrigger, Draggable);
  gsap.from(".four-blocks__item--icon", {
    scrollTrigger: {
      trigger: ".four-blocks",
      toggleActions: "restart pause restart pause",     

    },
    opacity: 0,
    scale:0.5,
    duration: 1,
    rotation: 360,    
    stagger: 0.25   
  });

  gsap.from(".story-board__item", {
    scrollTrigger: {
      trigger: ".story-board",
      toggleActions: "restart pause restart pause",     
      start:"top 60%",
      end: "bottom +=100"      
    },
    duration: 1,
    opacity: 0,
    xPercent:-100,    
    stagger: 0.25       
  })

  gsap.from(".gallery-main", {
    scrollTrigger: {
      trigger: ".gallery__content",
      toggleActions: "restart pause restart pause",     
      start:"top 70%",
      end: "bottom +=100"
    },
    duration: 1,
    opacity: 0,
    xPercent:-100,    
  })

  
  gsap.from(".gallery-item", {
    scrollTrigger: {
      trigger: ".gallery__content",
      toggleActions: "restart pause restart pause",     
      start:"top 60%",
      end: "bottom +=100"
    },
    duration: 1,
    opacity: 0,
    yPercent:-100,    
    stagger: 0.25 
  })
  
  gsap.from(".video-present__gallery-item", {
    scrollTrigger: {
      trigger: ".video-present__gallery-list",
      toggleActions: "restart pause restart pause",     
      start:"top 70%",
      end: "bottom +=100"
    },
    duration: 1,
    opacity: 0,
    xPercent:-100,    
    stagger: 0.25 
  })

    
  gsap.from(".jumbo-present__jumbo", {
    scrollTrigger: {
      trigger: ".jumbo-present",
      toggleActions: "restart pause restart pause",     
      start:"top 70%",
      end: "bottom +=100"
    },
    duration: 1,
    opacity: 0,
    xPercent:100,    
  })

      
  gsap.from(".jumbo-present__gallery-item", {
    scrollTrigger: {
      trigger: ".jumbo-present__gallery",
      toggleActions: "restart pause restart pause",     
      start:"top 70%",
      end: "bottom +=100",
    },
    duration: 1,
    opacity: 0,
    yPercent:100,    
    stagger: 0.25 
  })

  gsap.from(".gallery__item", {
    scrollTrigger: {
      trigger: ".gallery",
      toggleActions: "restart pause restart pause",     
      start:"top 90%",
      end: "bottom +=100",
    },
    duration: 1,
    opacity: 0,
    xPercent: -100,    
    stagger: 0.25 
  })

  gsap.from(".results__item", {
    scrollTrigger: {
      trigger: ".results__list",
      toggleActions: "restart pause restart pause",     
    },
    duration: 1,
    opacity: 0,
    scale: 0.1,    
    stagger: 0.25 
  })

  jQuery(function ($) {
    // $('.header-slider__list').slick({
    //   centerMode: true,
    //   centerPadding: '0',
    //   slidesToShow: 3,     
    // });
  });
});


i